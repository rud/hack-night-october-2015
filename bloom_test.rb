require 'test/unit'
require 'test/unit/assertions'
include Test::Unit::Assertions
require 'digest'
require 'pry'

class Bloom

  attr_reader :bloom_size, :terms_stored, :bits_set, :bit_collissions

  def initialize bloom_size: 10
    @bloom_size = bloom_size
    @bloom_data = Array.new(bloom_size, 0)
    @bits_set = 0
    @bit_collissions = 0
    @terms_stored = 0
  end

  def <<(term)
    @terms_stored += 1
    bits_for_term(term).each do |to_set|
      index = to_set % @bloom_size
      if @bloom_data[index] == 0
        @bits_set += 1
        @bloom_data[index] = 1
      else
        @bit_collissions += 1
      end
    end
  end

  def member?(term)
    bits_for_term(term).all? do |to_check|
      @bloom_data[to_check % @bloom_size] == 1
    end
  end

  # Return 4 32-bit integers for the term
  def bits_for_term(term)
    Digest::MD5.digest(term).
      unpack('N*')
  end
end


class BloomingTest < Test::Unit::TestCase

  def test_bits_for_term
    bloom = Bloom.new
    assert_equal(
      [2169925272, 1828244584, 1510321243, 1665857528],
      bloom.bits_for_term('cow')
    )
  end

  def test_small_setup
    words = [
      'car',
      'monkey',
      'mittens'
    ]
    bloom = Bloom.new(bloom_size: 50)
    assert_equal(0, bloom.bits_set)
    assert_equal(0, bloom.bit_collissions)

    words.each do |word|
      bloom << word
    end
    assert_equal(10, bloom.bits_set)
    assert_equal(2, bloom.bit_collissions)

    words.each do |word|
      assert bloom.member?(word)
    end

    refute bloom.member?('mitten')

    assert_equal 50, bloom.bloom_size
    assert_equal 3, bloom.terms_stored
  end

  def test_5000_first_of_wordlist
    bloom = Bloom.new(bloom_size: 50_000)
    test_words = []

    File.open('wordlist.txt', 'r') do |file|
      words = 5000
      while words > 0
        bloom << file.readline.strip
        words -= 1
      end

      words = 1000
      while words > 0
        test_words << file.readline.strip
        words -= 1
      end
    end

    assert_equal(5_000, bloom.terms_stored)
    assert_equal(16_432, bloom.bits_set)
    assert_equal(3_568, bloom.bit_collissions)

    assert_equal(1_000, test_words.length)

    false_posities = 0
    test_words.each do |term|
      if bloom.member? term
        false_posities += 1
      end
    end
    assert_equal(9, false_posities)
  end
end

Kernel.exit Test::Unit::AutoRunner.run
